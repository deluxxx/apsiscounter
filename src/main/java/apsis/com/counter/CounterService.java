package apsis.com.counter;

import java.util.List;

import apsis.com.counter.dto.ResponseCounterDto;

public interface CounterService {

	ResponseCounterDto getCounter(String counterName);

	List<ResponseCounterDto> getCounters();

	ResponseCounterDto addCounter(String counterName);

	ResponseCounterDto incrementCounterByName(String counterName);

}
