package apsis.com.counter;

import apsis.com.counter.exception.NotFoundException;
import apsis.com.counter.exception.NotUniqueCounterNameException;
import apsis.com.counter.exception.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(NotFoundException.class)
	public static ResponseEntity handleNotAuthorizedException(NotFoundException ex) {
		return new ResponseEntity<>(new Response(ex.getMessage()), HttpStatus.NOT_FOUND);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(NotUniqueCounterNameException.class)
	public static ResponseEntity handleNotUniqueCounterNameException(NotUniqueCounterNameException ex) {
		return new ResponseEntity<>(new Response(ex.getMessage()), HttpStatus.CONFLICT);
	}
}
