package apsis.com.counter;

import apsis.com.counter.dto.ResponseCounterDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/counter")
@Slf4j
public class CounterController {

	private final CounterServiceImpl counterServiceImpl;

	public CounterController(final CounterServiceImpl counterServiceImpl) {
		this.counterServiceImpl = counterServiceImpl;
	}

	@ApiOperation(value = "Get counter by name.", response = ResponseCounterDto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Counter object founded."),
			@ApiResponse(code = 404, message = "The object with the given name could not be found."),
			@ApiResponse(code = 500, message = "Internal server error occurred.")
	})
	@GetMapping("/{counterName}")
	public ResponseEntity getCounter(
			@PathVariable
			@ApiParam(name = "counterName", value = "The name of the counter to search for. Capitalization does not matter.")
					String counterName) {
		log.info("Method [getCounter], with variable [%s]", counterName);

		return ResponseEntity.ok(counterServiceImpl.getCounter(counterName));
	}

	@ApiOperation(value = "Increment counter by name. ", response = ResponseCounterDto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Counter object incremented."),
			@ApiResponse(code = 404, message = "The object with the given name could not be found."),
			@ApiResponse(code = 500, message = "Internal server error occurred.")
	})
	@PutMapping("/{counterName}")
	public ResponseEntity incrementCounterByName(
			@PathVariable
			@ApiParam(name = "counterName", value = "The name of the counter to search for. Capitalization does not matter.")
					String counterName) {
		log.info("Method [incrementCounterByName], with variable [%s]", counterName);
		return ResponseEntity.ok(counterServiceImpl.incrementCounterByName(counterName));
	}

	@ApiOperation(value = "Increment counter by name. ", response = ResponseCounterDto.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "All counters founded in system."),
			@ApiResponse(code = 500, message = "Internal server error occurred.")
	})
	@GetMapping("/")
	public ResponseEntity getCounters() {
		log.info("Method [getCounters]");
		return ResponseEntity.ok(counterServiceImpl.getCounters());
	}

	@ApiOperation(value = "Add new counter.", response = ResponseCounterDto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully created new counter"),
			@ApiResponse(code = 409, message = "A counter with this name exists."),
			@ApiResponse(code = 500, message = "Internal server error occurred.")
	})
	@PostMapping("/{counterName}")
	public ResponseEntity addCounter(@PathVariable String counterName) {
		log.info("Method [addCounter], with variable [%s]", counterName);
		return new ResponseEntity<>(counterServiceImpl.addCounter(counterName), HttpStatus.CREATED);
	}
}
