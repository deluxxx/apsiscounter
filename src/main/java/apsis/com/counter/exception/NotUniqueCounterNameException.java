package apsis.com.counter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class NotUniqueCounterNameException extends RuntimeException {

	public NotUniqueCounterNameException(String message) {
		super(message);
	}

}
