package apsis.com.counter.validation;

import java.util.concurrent.ConcurrentHashMap;

import apsis.com.counter.exception.NotUniqueCounterNameException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CounterValidation {

	public boolean isUniqueCounterName(String counterName, ConcurrentHashMap concurrentHashMap) {
		log.info("Method [isUniqueCounterName] by param [%s]", counterName);
		return concurrentHashMap.get(counterName) == null;
	}

	public boolean isUniqueCounterNameWithException(String counterName, ConcurrentHashMap concurrentHashMap) {
		boolean isValid = isUniqueCounterName(counterName, concurrentHashMap);
		log.info("Method [isUniqueCounterNameWithException] result: [%s]", isValid);
		if (!isValid) {
			throw new NotUniqueCounterNameException("The name of the new counter must be unique");
		}
		return isValid;
	}
}