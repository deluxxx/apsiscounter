package apsis.com.counter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

import apsis.com.counter.dto.ResponseCounterDto;
import apsis.com.counter.exception.NotFoundException;
import apsis.com.counter.validation.CounterValidation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CounterServiceImpl implements CounterService {

	ConcurrentHashMap<String, LongAdder> counters = new ConcurrentHashMap<>();

	private final CounterValidation counterValidation;

	public CounterServiceImpl(final CounterValidation counterValidation) {
		this.counterValidation = counterValidation;
	}

	public ResponseCounterDto getCounter(String counterName) {
		log.info("Method [getCounter] variables: [%s]", counterName);
		counterName = counterName.toUpperCase();
		LongAdder counter = counters.get(counterName);

		if (counter == null) {
			throw new NotFoundException(String.format("Not found counter named: [%s]", counterName));
		}

		return new ResponseCounterDto(counterName, counter.longValue());
	}

	public List<ResponseCounterDto> getCounters() {
		log.info("Method [getCounters]");
		List<ResponseCounterDto> results = new ArrayList<>();

		counters.forEach((name, counter) -> {
			results.add(new ResponseCounterDto(name, counter.longValue()));
		});

		return results;
	}

	public ResponseCounterDto addCounter(String counterName) {
		log.info("Method [addCounter] variables: [%s]", counterName);

		counterName = counterName.toUpperCase(Locale.ENGLISH);
		counterValidation.isUniqueCounterNameWithException(counterName, counters);
		counters.put(counterName, new LongAdder());

		return new ResponseCounterDto(counterName, 0L);
	}

	public ResponseCounterDto incrementCounterByName(String counterName) {
		log.info("Method [incrementCounterByName] variables: [%s]", counterName);

		counterName = counterName.toUpperCase();
		LongAdder counter = counters.get(counterName);
		counters.computeIfAbsent(counterName, k -> new LongAdder()).increment();

		if (counter == null) {
			throw new NotFoundException(String.format("Not found counter named: [%s]", counterName));
		}

		return new ResponseCounterDto(counterName, counter.longValue());
	}

}
