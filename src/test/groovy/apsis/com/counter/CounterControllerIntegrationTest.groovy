package apsis.com.counter

import apsis.com.counter.dto.ResponseCounterDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.TestPropertySource
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application_test.properties")
class CounterControllerIntegrationTest extends Specification {

	@LocalServerPort
	private int port

	@Autowired
	private TestRestTemplate restTemplate

	void "should return 4 counters"() {
		given:
			restTemplate.postForEntity(createURLWithPort("/{counterName}"), null, String.class, "Counter 1")
			restTemplate.postForEntity(createURLWithPort("/{counterName}"), null, String.class, "Counter 2")
			restTemplate.postForEntity(createURLWithPort("/{counterName}"), null, String.class, "Counter 3")
			restTemplate.postForEntity(createURLWithPort("/{counterName}"), null, String.class, "Counter 4")
		when:

			def responseGet = restTemplate.exchange(createURLWithPort("/"), HttpMethod.GET, null, new ParameterizedTypeReference<List<ResponseCounterDto>>() {})
		then:
			responseGet.getBody().size() == 4
	}

	void "should throw not found exception by not exist counter name"() {
		when:
			def response = restTemplate.getForEntity(createURLWithPort("/{counterName}"), String.class, "Not Exist Counter")
		then:
			response.getStatusCode() == HttpStatus.NOT_FOUND
	}

	void "should add counter"() {
		given:
			final def NAME_TO_ADD = "Calm counter"

		when:
			def response = restTemplate.postForEntity(createURLWithPort("/{counterName}"), null, String.class, NAME_TO_ADD)

		then:
			response.getStatusCode() == HttpStatus.CREATED
	}

	void "should return counter by name"() {
		given:
			final def COUNTER_NAME = "CRAZY COUNTER"
			restTemplate.postForEntity(createURLWithPort("/{counterName}"), null, String.class, COUNTER_NAME)
		when:
			def response = restTemplate.getForObject(createURLWithPort("/{counterName}"), ResponseCounterDto.class, COUNTER_NAME)
		then:
			response.getCounter() == 0
			response.getName() == COUNTER_NAME
	}

	void "should increment counter by name"() {
		given:
			final def COUNTER_NAME = "FUZZY COUNTER"
			restTemplate.postForEntity(createURLWithPort("/{counterName}"), null, String.class, COUNTER_NAME)
		when:
			restTemplate.put(createURLWithPort("/{counterName}"), null, COUNTER_NAME)
			restTemplate.put(createURLWithPort("/{counterName}"), null, COUNTER_NAME)
			restTemplate.put(createURLWithPort("/{counterName}"), null, COUNTER_NAME)
			restTemplate.put(createURLWithPort("/{counterName}"), null, COUNTER_NAME)
			def response = restTemplate.getForObject(createURLWithPort("/{counterName}"), ResponseCounterDto.class, COUNTER_NAME)
		then:
			response.getCounter() == 4
			response.getName() == COUNTER_NAME
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + "/counter" + uri
	}

}
