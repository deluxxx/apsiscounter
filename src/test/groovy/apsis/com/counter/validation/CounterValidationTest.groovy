package apsis.com.counter.validation

import java.util.concurrent.ConcurrentHashMap

import apsis.com.counter.exception.NotUniqueCounterNameException
import spock.lang.Specification

class CounterValidationTest extends Specification {


	CounterValidation counterValidation = new CounterValidation()


	def "should return correct validation when it is not found in the database"() {
		given:
			ConcurrentHashMap<String, Long> concurrentHashMap = new ConcurrentHashMap<>()
		when:
			def result = counterValidation.isUniqueCounterName("test", concurrentHashMap)

		then:
			result
	}

	def "should return incorrect validation when it is found in the database"() {
		given:
			ConcurrentHashMap<String, Long> concurrentHashMap = new ConcurrentHashMap<>()
			concurrentHashMap.put("test", 0)
		when:
			def result = counterValidation.isUniqueCounterName("test", concurrentHashMap)

		then:
			result == false
	}

	def "should throw exception when it is found in the database"() {
		given:
			ConcurrentHashMap<String, Long> concurrentHashMap = new ConcurrentHashMap<>()
			concurrentHashMap.put("test", 0)
		when:
			counterValidation.isUniqueCounterNameWithException("test", concurrentHashMap)

		then:
			thrown NotUniqueCounterNameException
	}

	def "should return correct when it is not found in the database"() {
		given:
			ConcurrentHashMap<String, Long> concurrentHashMap = new ConcurrentHashMap<>()
		when:
			def result = counterValidation.isUniqueCounterNameWithException("test", concurrentHashMap)

		then:
			result
	}

}