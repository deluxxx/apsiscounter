package apsis.com.counter

import apsis.com.counter.exception.NotFoundException
import apsis.com.counter.exception.NotUniqueCounterNameException
import apsis.com.counter.validation.CounterValidation
import spock.lang.Specification

class CounterServiceTest extends Specification {


	CounterValidation counterValidation = new CounterValidation()

	CounterService counterService = new CounterServiceImpl(counterValidation)

	def "should return counter by name"() {
		given:
			final def COUNTER_NAME = "Amazing Counter"
			counterService.addCounter(COUNTER_NAME)
		when:
			def counter = counterService.getCounter(COUNTER_NAME)

		then:
			counter != null
	}

	def "should throw exception if counter by name not exist"() {
		when:
			counterService.getCounter("Counter 1")

		then:
			thrown NotFoundException
	}

	def "should return 5 counters"() {
		given:
			counterService.addCounter("Country 1")
			counterService.addCounter("Country 2")
			counterService.addCounter("Country 3")
			counterService.addCounter("Country 4")
			counterService.addCounter("Country 5")

		when:
			def counters = counterService.getCounters()

		then:
			counters.size() == 5
	}

	def "should add new counter"() {
		given:
			final def COUNTER_NAME = "AMAZING COUNTER"
		when:
			def counter = counterService.addCounter(COUNTER_NAME)

		then:
			counter.getName() == COUNTER_NAME
			counter.getCounter() == 0
	}

	def "should throw exception if counter by name exist"() {
		given:
			final def COUNTER_NAME = "AMAZING COUNTER"
			counterService.addCounter(COUNTER_NAME)
		when:
			counterService.addCounter(COUNTER_NAME)

		then:
			thrown NotUniqueCounterNameException
	}

}